package nipon.com.recyclerviewwithloadmoreprogressbar;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements OnLoadMoreListener{

    ArrayList<HashMap<String, String>> getDatalist;
    private RecyclerView mrecyclerView;
    RecyclerViewAdapter mAdapter;
    private Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        random = new Random();

        getDatalist = new ArrayList<>();
        for (int aind = 0; aind < 20; aind++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("KEY_EMAIL", "android" + aind + "@gmail.com");
            map.put("KEY_PHONE", phoneNumberGenerating());
            getDatalist.add(map);
        }

        mrecyclerView = findViewById(R.id.recyclerView);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));

        mAdapter = new RecyclerViewAdapter(MainActivity.this, getDatalist, mrecyclerView);
        mrecyclerView.setAdapter(mAdapter);


        mAdapter.setOnLoadMoreListener(this);
    }



    @Override
    public void onLoadMore() {
        getDatalist.add(null);
        mAdapter.notifyItemInserted(getDatalist.size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDatalist.remove(getDatalist.size() - 1);
                mAdapter.notifyItemRemoved(getDatalist.size());

                //Generating more data
                int index = getDatalist.size();
                int end = index + 20;
                for (int i = index; i < end; i++) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("KEY_EMAIL", "android" + i + "@gmail.com");
                    map.put("KEY_PHONE", phoneNumberGenerating());
                    getDatalist.add(map);
                }
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
            }
        }, 5000);
    }



    private String phoneNumberGenerating() {
        int low = 100000000;
        int high = 999999999;
        int randomNumber = random.nextInt(high - low) + low;

        return "0" + randomNumber;
    }

}

